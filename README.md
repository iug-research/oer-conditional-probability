![](https://licensebuttons.net/l/by/4.0/88x31.png)

# A Digital Learning Environment for "Conditional Probability"

(German description below)

This web-based digital learning environment offers instruction on conditional probability in both English and German. The course provides a comprehensive learning experience, featuring textual content (with text-to-speech functionality), interactive visualizations, and auto-generated exercises to guide learners through the concepts of conditional probability and conditional independence.

# Eine digitale Lernumgebung zum Thema "bedingte Wahrscheinlichkeit"

In dieser webbasierten digitalen Lernumgebung werden Lerninhalte zum Thema "bedingte Wahrscheinlichkeit" sowohl auf Englisch als auch auf Deutsch angeboten. Der Kurs bietet eine abwechslungsreiche Lernerfahrung, die Textinhalte (mit Text-zu-Sprache-Funktion), interaktive Visualisierungen und automatisch generierte Übungen umfasst, um die Lernenden durch die Konzepte der bedingten Wahrscheinlichkeit und der bedingten Unabhängigkeit zu führen.

# License

[A Digital Learning Environment for "Conditional Probability"](https://iug-research.gitlab.io/oer-conditional-probability) by [Research Group Informatik & Gesellschaft (HTW Berlin)](https://iug.htw-berlin.de/) is licensed under [CC BY 4.0](http://creativecommons.org/licenses/by/4.0/?ref=chooser-v1) <img height="22" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img height="22" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1">

# Citing this Work

When referencing this Open Educational Resource in scientific publications, kindly attribute it by citing the following source:

> Selmanagić, André; Simbeck, Katharina (2023): Breaking It Down: On the Presentation of Fine-Grained Learning Objects in Virtual Learning Environments. In : 21. Fachtagung Bildungstechnologien (DELFI). Bonn: Gesellschaft für Informatik e.V, pp. 155–160. [https://doi.org/10.18420/delfi2023-25](https://doi.org/10.18420/delfi2023-25)